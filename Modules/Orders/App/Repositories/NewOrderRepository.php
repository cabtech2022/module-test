<?php

namespace Modules\Orders\App\Repositories;

use Modules\Orders\App\Interfaces\OrderRepositoryInterface;
use Modules\Orders\App\Models\Orders;

class NewOrderRepository extends OrderRepository implements OrderRepositoryInterface 
{
    public function getAllOrders()
    {
        return [
            1, 2, 3
        ];
    }
}