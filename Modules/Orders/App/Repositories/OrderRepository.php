<?php

namespace Modules\Orders\App\Repositories;

use Modules\Orders\App\Interfaces\OrderRepositoryInterface;
use Modules\Orders\App\Models\Orders;

class OrderRepository implements OrderRepositoryInterface 
{
    protected $model;
    public function __construct(Orders $model) 
    {
        $this->model = $model;
    }
    public function getAllOrders() 
    {
        return $this->model->all();
    }

    public function getOrderById($orderId) 
    {
        return Orders::findOrFail($orderId);
    }

    public function deleteOrder($orderId) 
    {
        Orders::destroy($orderId);
    }

    public function createOrder(array $orderDetails) 
    {
        return Orders::create($orderDetails);
    }

    public function updateOrder($orderId, array $newDetails) 
    {
        return Orders::whereId($orderId)->update($newDetails);
    }

    public function getFulfilledOrders() 
    {
        return Orders::where('is_fulfilled', true);
    }
}