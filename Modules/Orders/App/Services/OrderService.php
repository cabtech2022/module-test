<?php
namespace Modules\Orders\App\Services;

use Modules\Orders\App\Interfaces\OrderRepositoryInterface;
use Modules\Orders\App\Repositories\OrderRepository;

class OrderService
{
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getAllOrders()
    {
        return $this->orderRepository->getAllOrders();
    }

    public function getOrderById($orderId)
    {
        return $this->orderRepository->getOrderById($orderId);
    }

    public function deleteOrder($orderId)
    {
        return $this->orderRepository->deleteOrder($orderId);
    }

    public function createOrder(array $orderDetails)
    {
        return $this->orderRepository->createOrder($orderDetails);
    }

    public function updateOrder($orderId, array $newDetails)
    {
        return $this->orderRepository->updateOrder($orderId, $newDetails);
    }

    public function getFulfilledOrders()
    {
        return $this->orderRepository->getFulfilledOrders();
    }
}