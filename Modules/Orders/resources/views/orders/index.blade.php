@extends('layouts.master') <!-- You may need to adjust this based on your layout file -->

@section('content')
    <h1>Orders</h1>

    <ul>
        @foreach($orders as $order)
            <li>{{ $order->client }} - {{ $order->details }}</li>
        @endforeach
    </ul>
@endsection